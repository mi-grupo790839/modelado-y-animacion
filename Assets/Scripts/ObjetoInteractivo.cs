using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoInteractivo : MonoBehaviour
{
public bool doorOpen; 
public float doorOpenAngle;
public float doorCloseAngle;
public float smooth = 3.0f;
    public void ActivarObjeto()
    {
        doorOpen = !doorOpen;
    }

void Update()
{
    if (doorOpen)
    {
        Quaternion targetRotation = Quaternion.Euler(0, doorOpenAngle, 0);
transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, smooth * Time.deltaTime);
    }
    else
    {
        Quaternion targetRotation2 = Quaternion.Euler(0, doorCloseAngle, 0);
transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation2, smooth * Time.deltaTime);
    }
}
}

